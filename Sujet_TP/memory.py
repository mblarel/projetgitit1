import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    """
    Fonction qui mélange les cartes.
    
    :param Tab: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :CU: Tab doit contenir des chaines de caractères.
    :return: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :Example:
    >>> Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']
    >>> new_Tabl = melange_carte(Tabl)
    >>> Tabl[0] in new_Tabl
    True
    """
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[k], Tab[i]
    return Tab

def carte_cache(Tab):   #TODO
    """
    Fonction qui crée une liste de même longueur que la liste renvoyées dans la question précédentes.
    
    :param Tab: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :CU: Tab doit contenir des chaines de caractères.
    :return: (list) une liste d'une chaîne de caractères représentant un autre tableau de cartes retournées (de dos).
    :Example:
    >>> Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']
    >>> new_Tabl = carte_cache(Tabl)
    >>> len(new_Tabl) == len(Tabl)
    True
    """


def choisir_cartes(Tab):
    """
    Fonction qui fait choisir au joueur deux cartes.
    
    :param Tab: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :CU: Tab doit contenir des chaines de caractères.
    :return: (list) une liste de deux chaînes de caractères représentant deux cartes choisies. Cette fonction affiche aussi les choix des fonctions.
    :Example:
    >>> Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']
    >>> cartes = choisir_cartes(Tabl)
    >>> len(cartes) == 2
    True
    >>> cartes[0] in Tabl
    True
    >>> cartes[1] in Tabl
    True
    """
    c1 = int(input("Choisissez une carte : "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(inpt("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
    Fonction qui retroune les cartes dans la liste cachée.
    
    :param c1: (string) une chaîne de caractères représentant une carte.
    :param c2: (string) une chaîne de caractères représentant une seconde carte.
    :param Tab: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :param Tab_cache: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :CU: Tab doit contenir des chaines de caractères. c1 et c2 doivent être dans le tableau Tabl. len(Tabl) == len(Tab_cache).
    :return: (string) une chaîne de caractère représentant la carte que l'on souhiate retournée.
    """

def jouer(Tab):
    """
    Fonction qui fait jouer les joueurs en affichant des messages.
        
    :param Tab: (list) une liste d'une chaîne de caractères représentant un tableau de cartes.
    :CU: Tab doit contenir des chaines de caractères. c1 et c2 doivent être dans le tableau Tabl. len(Tabl) == len(Tab_cache).
    :return: (None) Cette fonction affiche le déroulement du jeu.
    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

    
jouer(Tabl)